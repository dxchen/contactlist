package com.example.david.contactlist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.example.david.contactlist.R;
import com.example.david.contactlist.app.AppController;
import com.example.david.contactlist.object.Contact;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;


/**
 * Created by David on 5/29/2015.
 */
public class DetailViewActivity extends AppCompatActivity{

    /**
     * Generates entire page on create
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        Contact contact = b.getParcelable("contact");

        String url = contact.getDetails();
        final ImageLoader imageLoader = AppController.getInstance().getImageLoader();

        TextView name = (TextView) findViewById(R.id.name);
        TextView company = (TextView) findViewById(R.id.company);
        TextView phone = (TextView) findViewById(R.id.phone);
        TextView birthday = (TextView) findViewById(R.id.birthday);
        final TextView email = (TextView) findViewById(R.id.email);
        final TextView address = (TextView) findViewById(R.id.address);
        final TextView website = (TextView) findViewById(R.id.website);
        final NetworkImageView image = (NetworkImageView)
                findViewById(R.id.image);

        name.setText(contact.getName());
        company.setText(contact.getCompany());
        phone.setText("(Work)"+ contact.getWorkNumber() + "\n(Home)"+ contact.getHomeNumber() + "\n(Mobile)"+ contact.getMobileNumber());


        String birthdayString = new SimpleDateFormat("MMMM dd, yyyy").format(contact.getBirthDate ()*1000);//converts unixtime to readable format
        birthday.setText(birthdayString);
        /**
         * Gets more details from the detailsURL
         */
        JsonObjectRequest req = new JsonObjectRequest
                ( Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            String emailString = response.getString("email");
                            JSONObject addressObj = response.getJSONObject("address");
                            String addressString = addressObj.getString("street") + "\n" + addressObj.getString("city") + ", " + addressObj.getString("state") + " " + addressObj.getString("zip");
                            String imageURL = response.getString("largeImageURL");
                            String siteURL = response.getString("website");

                            email.setText(emailString);
                            address.setText(addressString);
                            image.setImageUrl(imageURL, imageLoader);
                            website.setText(siteURL);

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                }
    }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        AppController.getInstance().addToRequestQueue(req);

    }
}
