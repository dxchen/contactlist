package com.example.david.contactlist.util;

import com.android.volley.toolbox.ImageLoader.ImageCache;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
/**
 * Created by David on 5/29/2015.
 * Caches images to be stored on disk
 */
public class LruBitmapCache extends LruCache<String, Bitmap> implements
        ImageCache {
    /**
     * Fetches default cache size of phone
     * @return size of the cache as an int
     */
    public static int getDefaultLruCacheSize() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        return cacheSize;
    }

    /**
     * basic constructor, uses phones basic cache size
     */
    public LruBitmapCache() {
        this(getDefaultLruCacheSize());
    }

    /**
     * constructor with size as input
     * @param sizeInKiloBytes - amount of data set aside
     */
    public LruBitmapCache(int sizeInKiloBytes) {
        super(sizeInKiloBytes);
    }

    /**
     * returns the size of specific bitmap
     * @param key
     * @param value
     * @return
     */
    @Override
    protected int sizeOf(String key, Bitmap value) {
        return value.getRowBytes() * value.getHeight() / 1024;
    }

    /**
     * retrieves the bitmap at url from cache
     * @param url
     * @return bitmap at url
     */
    @Override
    public Bitmap getBitmap(String url) {
        return get(url);
    }

    /**
     * Adds or replaces bitmap at url in cache
     * @param url
     * @param bitmap
     */
    @Override
    public void putBitmap(String url, Bitmap bitmap) {
        put(url, bitmap);
    }
}
