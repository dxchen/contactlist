package com.example.david.contactlist.object;

import android.os.Parcel;
import android.os.Parcelable;



/**
 * Created by David on 5/28/2015.
 * Used for saving parsed data and providing it to list view
 */
public class Contact implements Parcelable{
    private String name;
    private int id;
    private String company;
    private String detailsURL;
    private String smallImageURL;
    private long birthDate;//Unix time in seconds
    private String workNumber;
    private String homeNumber;
    private String mobileNumber;

    /**
     * Basic constructor that takes multiple inputs
     * @param name
     * @param id
     * @param company
     * @param detailsURL
     * @param smallImageURL
     * @param birthDate
     * @param workNumber
     * @param homeNumber
     * @param mobileNumber
     */
    public Contact(String name, int id, String company, String detailsURL, String smallImageURL,long birthDate, String workNumber, String homeNumber, String mobileNumber){
        this.name = name;
        this.id = id;
        this.company = company;
        this.detailsURL = detailsURL;
        this.smallImageURL = smallImageURL;
        this.birthDate = birthDate;
        this.workNumber = workNumber;
        this.homeNumber = homeNumber;
        this.mobileNumber = mobileNumber;
    }

    /**
     * Makes contact out of Parcel
     * @param in
     */
    public Contact(Parcel in){
        String[] data = new String[9];
        in.readStringArray(data);
        this.name = data[0];
        this.id = Integer.parseInt(data[1]);
        this.company = data[2];
        this.detailsURL = data[3];
        this.smallImageURL = data[4];
        this.birthDate = Long.parseLong(data[5]);
        this.workNumber = data[6];
        this.homeNumber = data[7];
        this.mobileNumber = data[8];
    }
    @Override
    public int describeContents() {
        return 0;
    }
    public void writeToParcel(Parcel dest, int flags){
        dest.writeStringArray(new String[]{name, String.valueOf(id), company, detailsURL, smallImageURL, String.valueOf(birthDate), workNumber, homeNumber, mobileNumber});
    }
    public static final Parcelable.Creator<Contact> CREATOR
            = new Parcelable.Creator<Contact>() {

        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
    /**
     * Various getters for the stored data.
     */
    public String getName(){
        return name;
    }
    public int getID(){
        return id;
    }
    public String getCompany(){
        return company;
    }
    public String getDetails(){
        return detailsURL;
    }
    public String getThumbnail(){
        return smallImageURL;
    }
    public long getBirthDate(){
        return birthDate;
    }
    public String getWorkNumber(){
        return  workNumber;
    }
    public String getHomeNumber(){
        return homeNumber;
    }
    public String getMobileNumber(){
        return mobileNumber;
    }


}
