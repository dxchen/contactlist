package com.example.david.contactlist;

import android.app.Application;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.david.contactlist.activity.ListActivity;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ActivityInstrumentationTestCase2<ListActivity> {

    private static final String TAG = "ActivityTest";
    private static boolean flagSetUp = true;
    private static boolean flagTearDown = false;
    private ListActivity testActivity;
    private ListView testContactList;
    public ApplicationTest(){super(ListActivity.class);}
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Log.d(TAG, "setUp");

        if (flagSetUp) {
            flagSetUp = false;
        }
        testActivity = getActivity();
        testContactList = (ListView)testActivity.findViewById(R.id.list);
    }
    @SmallTest
    public void test0Preconditions() {
        Log.d(TAG, "test0Preconditions");
        assertNotNull("testActivity is null", testActivity);
        assertNotNull("testContactList is null", testContactList);
    }
    @SmallTest
    public void test1CountContacts() {
        Log.d(TAG, "test1CountContacts");
        assertEquals(testContactList.getCount(),20);
    }

}