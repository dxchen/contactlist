package com.example.david.contactlist.activity;

import android.app.ProgressDialog;
import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.*;
import com.android.volley.toolbox.*;
import com.example.david.contactlist.R;
import com.example.david.contactlist.adapter.CustomListAdapter;
import com.example.david.contactlist.app.AppController;
import com.example.david.contactlist.object.Contact;

import org.json.*;

import java.util.ArrayList;
import java.util.List;


public class ListActivity extends AppCompatActivity {
    private static final String TAG = ListActivity.class.getSimpleName();

    private String url = "https://solstice.applauncher.com/external/contacts.json";
    private List<Contact> contactList = new ArrayList<Contact>();
    private ProgressDialog pDialog;
    private ListView listView;
    private CustomListAdapter adapter;
    /**
     * When the activity is created I use volley to get the json file and parse it into usable contact data.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listView = (ListView) findViewById(R.id.list);
        adapter = new CustomListAdapter(this, contactList);
        listView.setAdapter(adapter);
        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();



        //retrieves and parses the JSONArray, storing it in an array of Contacts
        JsonArrayRequest req = new JsonArrayRequest
                ( url, new Response.Listener<JSONArray>() {
                    //parsing the json array
                    @Override
                    public void onResponse(JSONArray response) {

                            hidePDialog();

                            for(int i = 0; i < response.length();i++){
                                try{

                                    JSONObject person = response.getJSONObject(i);
                                    JSONObject phone = person.getJSONObject("phone");
                                    String work = phone.getString("work");
                                    String home = phone.getString("home");
                                    String mobile;
                                    try {// for catching if the user doesn't have a mobile phone
                                        mobile = phone.getString("mobile");
                                    }catch (JSONException e) {
                                        e.printStackTrace();
                                        mobile = "";
                                    }
                                    contactList.add( new Contact(
                                        person.getString("name"),
                                        person.getInt("employeeId"),
                                        person.getString("company"),
                                        person.getString("detailsURL"),
                                        person.getString("smallImageURL"),
                                        person.getLong("birthdate"),
                                            work,
                                            home,
                                            mobile));
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }
                            }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidePDialog();
                    }
                });
        AppController.getInstance().addToRequestQueue(req);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(),DetailViewActivity.class);
                intent.putExtra("contact", contactList.get(position));
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
}
