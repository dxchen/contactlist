package com.example.david.contactlist.adapter;

import com.example.david.contactlist.app.AppController;
import com.example.david.contactlist.R;
import com.example.david.contactlist.object.Contact;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
/**
 * Created by David on 5/29/2015.
 * Custom adapter for loading data into the listview
 */
public class CustomListAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Contact> contacts;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    /**
     * constructor
     * @param activity
     * @param contacts
     */
    public CustomListAdapter(Activity activity, List<Contact> contacts) {
        this.activity = activity;
        this.contacts = contacts;
    }

    /**
     * @return number of contacts
     */
    @Override
    public int getCount() {
        return contacts.size();
    }

    /**
     *
     * @param location
     * @return returns contact at location
     */
    @Override
    public Object getItem(int location) {
        return contacts.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Creates the contact row
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.contact_row, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView work = (TextView) convertView.findViewById(R.id.work);
        TextView home = (TextView) convertView.findViewById(R.id.home);
        TextView mobile = (TextView) convertView.findViewById(R.id.mobile);
        // getting contact data for the row
        Contact c = contacts.get(position);

        // thumbnail image
        thumbNail.setImageUrl(c.getThumbnail(), imageLoader);

        // name
        name.setText(c.getName());
        //phone
        work.setText("Work: "+ c.getWorkNumber());
        home.setText("Home: "+ c.getHomeNumber());
        mobile.setText("Mobile: "+ c.getMobileNumber());
        return convertView;
    }

}


